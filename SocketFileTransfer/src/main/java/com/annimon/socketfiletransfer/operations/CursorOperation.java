package com.annimon.socketfiletransfer.operations;

import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.annimon.socketfiletransfer.OperationListener;
import com.annimon.socketfiletransfer.R;
import com.annimon.socketfiletransfer.TouchpadView;
import com.annimon.socketfiletransfer.util.ExceptionHandler;

import java.io.IOException;

/**
 * @author aNNiMON
 */
public class CursorOperation extends Operation {

    private static final int DIVIDER = 3;
    private static final int
            NONE = -1,
            TYPE_MOVE = 1,
            TYPE_CLICK = 2,
            TYPE_DRAG = 3,
            TYPE_RELEASED = 4,
            TYPE_KEY_RELEASED = 5,
            TYPE_MOVE_RELATIVE = 6,
            TYPE_MOUSE_PRESSED = 7,
            TYPE_MOUSE_RELEASED = 8,
            STOP = 10;

    private boolean running;
    private int startX, startY;

    @Override
    public void startServerSide() throws Exception {

    }

    @Override
    public void startClientSide(Object... params) throws Exception {
        TouchpadView view = (TouchpadView) params[0];
        for (int i = 1; i <= 3; i++) {
//            ((Button)params[i]).setOnClickListener(clickListener);
            ((Button)params[i]).setOnTouchListener(buttonTouchListener);
        }
        running = true;

        dos.writeInt(OperationListener.MODE_CURSOR_CONTROL);

        // Android touchpad needs to send particular signal
        dos.writeInt(-100);
        dos.writeInt(-100);

        view.requestFocus();
        view.setOnTouchListener(touchListener);
        view.setOnKeyListener(keyListener);

        while(running) {
            Thread.sleep(10);
        }

        view.closeActivity();
    }

    private final View.OnTouchListener buttonTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int buttonMask = 0;
            switch (v.getId()) {
                case R.id.left_button:
                    buttonMask = 16;
                    break;
                case R.id.middle_button:
                    buttonMask = 8;
                    break;
                case R.id.right_button:
                    buttonMask = 4;
                    break;
                default: return true;
            }

            final int action = event.getAction() & MotionEvent.ACTION_MASK;
            int type = 0;
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    type = TYPE_MOUSE_PRESSED;
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    type = TYPE_MOUSE_RELEASED;
                    break;
                default: return true;
            }

            try {
                dos.writeInt(type);
                dos.writeInt(buttonMask);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }
    };

    private final View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (!running) return true;

            final int x = (int) motionEvent.getX();
            final int y = (int) motionEvent.getY();
            final int action = motionEvent.getAction() & MotionEvent.ACTION_MASK;

            if (action == MotionEvent.ACTION_DOWN) {
                startX = x;
                startY = y;
            } else if ( (action == MotionEvent.ACTION_UP) || (action == MotionEvent.ACTION_CANCEL) ) {
                try {
                    dos.writeInt(TYPE_RELEASED);
                } catch (IOException ex) {  }
            } else if (action == MotionEvent.ACTION_MOVE) {
                try {
                    dos.writeInt(TYPE_MOVE_RELATIVE);
                    dos.writeInt((x - startX) / DIVIDER);
                    dos.writeInt((y - startY) / DIVIDER);
                } catch (IOException ex) { }
            }

            return true;
        }
    };

    private final View.OnKeyListener keyListener = new View.OnKeyListener() {

        @Override
        public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
            if (!running) return true;

            if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                try {
                    dos.writeInt(TYPE_KEY_RELEASED);
                    dos.writeInt(convertToAscii(keyCode));
                } catch (IOException e) {
                    ExceptionHandler.log(e);
                }

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    running = false;
                }
            }
            return true;
        }

        private int convertToAscii(int key) {
            if (key >= KeyEvent.KEYCODE_A && key <= KeyEvent.KEYCODE_Z)
                return key - KeyEvent.KEYCODE_A + 'A';
            else if (key >= KeyEvent.KEYCODE_0 && key <= KeyEvent.KEYCODE_9)
                return key - KeyEvent.KEYCODE_0 + '0';
            else if (key == KeyEvent.KEYCODE_ENTER) return 10;
            else if (key == KeyEvent.KEYCODE_SHIFT_LEFT ||
                     key == KeyEvent.KEYCODE_SHIFT_RIGHT) return 0x16;
            else if (key == KeyEvent.KEYCODE_SPACE) return 32;
            else if (key == KeyEvent.KEYCODE_DEL) return 8; // Backspace
            else if (key == KeyEvent.KEYCODE_MENU) return 525; // Context Menu
            else if (key == KeyEvent.KEYCODE_VOLUME_UP) return 33; // Page up
            else if (key == KeyEvent.KEYCODE_VOLUME_DOWN) return 34; // Page down
            else if (key == KeyEvent.KEYCODE_DPAD_LEFT) return 37;
            else if (key == KeyEvent.KEYCODE_DPAD_UP) return 38;
            else if (key == KeyEvent.KEYCODE_DPAD_RIGHT) return 39;
            else if (key == KeyEvent.KEYCODE_DPAD_DOWN) return 40;
            else return key;
        }
    };
}
