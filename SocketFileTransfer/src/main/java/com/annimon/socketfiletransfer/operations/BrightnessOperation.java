package com.annimon.socketfiletransfer.operations;

import android.os.Message;

import com.annimon.socketfiletransfer.OperationListener;
import com.annimon.socketfiletransfer.ServerActivity;

/**
 * @author aNNiMON
 */
public class BrightnessOperation extends Operation {

    @Override
    public void startServerSide() throws Exception {
        String value = dis.readUTF();

        int brightness;
        try {
            brightness = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            brightness = 30;
        }

        if (handler != null) {
            Message msg = new Message();
            msg.what = ServerActivity.CHANGE_BRIGHTNESS;
            msg.arg1 = brightness;
            handler.sendMessage(msg);
        }
    }

    @Override
    public void startClientSide(Object... params) throws Exception {
        String value = (String) params[0];

        dos.writeInt(OperationListener.MODE_BRIGHTNESS_CHANGE);
        dos.writeUTF(value);
    }
}
