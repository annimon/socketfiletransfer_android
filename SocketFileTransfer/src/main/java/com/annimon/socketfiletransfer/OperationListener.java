package com.annimon.socketfiletransfer;

import com.annimon.socketfiletransfer.operations.BrightnessOperation;
import com.annimon.socketfiletransfer.operations.CursorOperation;
import com.annimon.socketfiletransfer.operations.FileOperation;
import com.annimon.socketfiletransfer.operations.MessageOperation;
import com.annimon.socketfiletransfer.operations.Operation;
import com.annimon.socketfiletransfer.util.ExceptionHandler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Прослушивание операций с данными.
 * @author aNNiMON
 */
public class OperationListener {

    public static final int
            MODE_SERVER = 0,
            MODE_FILE_TRANSFER = 1,
            MODE_MESSAGE_TRANSFER = 2,
            MODE_CURSOR_CONTROL = 3,
            MODE_BRIGHTNESS_CHANGE = 4;

    protected DataInputStream dis;
    protected DataOutputStream dos;

    public void setSocket(Socket socket) {
        try {
            dis = new DataInputStream( socket.getInputStream() );
            dos = new DataOutputStream( socket.getOutputStream() );
        } catch (IOException ex) {
            ExceptionHandler.log(ex);
        }
    }

    public void listenOperation() throws Exception {
        int mode = dis.readInt();
        Operation operation = getOperation(mode);
        if (operation != null) {
            operation.setDataInputStream(dis);
            operation.startServerSide();
        }
    }

    public void close() {
        if (dis != null) {
            try {
                dis.close();
            } catch (IOException ex) {
                ExceptionHandler.log(ex);
            }
        }
        if (dos != null) {
            try {
                dos.close();
            } catch (IOException ex) {
                ExceptionHandler.log(ex);
            }
        }
    }

    protected Operation getOperation(int mode) {
        Operation operation = null;
        switch(mode) {
            case MODE_FILE_TRANSFER:
                operation = new FileOperation();
                break;
            case MODE_MESSAGE_TRANSFER:
                operation = new MessageOperation();
                break;
            case MODE_CURSOR_CONTROL:
                operation = new CursorOperation();
                break;
            case MODE_BRIGHTNESS_CHANGE:
                operation = new BrightnessOperation();
                break;
        }

        return operation;
    }
}
