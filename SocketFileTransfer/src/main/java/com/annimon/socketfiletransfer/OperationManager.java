package com.annimon.socketfiletransfer;

import com.annimon.socketfiletransfer.operations.Operation;

/**
 * Manage data operations.
 * @author aNNiMON
 */
public class OperationManager extends OperationListener {

    public void execute(int mode, Object... params) throws Exception {
        Operation operation = getOperation(mode);
        if (operation != null) {
            operation.setDataOutputStream(dos);
            operation.startClientSide(params);
        }
    }

}