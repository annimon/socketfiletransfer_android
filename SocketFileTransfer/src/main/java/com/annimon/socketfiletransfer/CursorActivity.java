package com.annimon.socketfiletransfer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 *
 * @author aNNiMON
 */
public class CursorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set fullscreen mode.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                             WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Set backlight always on.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                             WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_cursor);

        TouchpadView view = (TouchpadView) findViewById(R.id.touchpad);
        view.requestFocus();

        Button leftButton = (Button) findViewById(R.id.left_button);
        Button middleButton = (Button) findViewById(R.id.middle_button);
        Button rightButton = (Button) findViewById(R.id.right_button);

        new SocketTransferTask(OperationListener.MODE_CURSOR_CONTROL).execute(view, leftButton, middleButton, rightButton);
    }
}
