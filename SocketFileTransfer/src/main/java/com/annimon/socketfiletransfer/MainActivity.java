package com.annimon.socketfiletransfer;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.annimon.socketfiletransfer.util.Configuration;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // We don't need to show UI if sharing data.
        if (checkSharingData()) {
            finish();
        }

        LayoutParams layoutParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(layoutParam);

        // Create main menu
        String[] items = getResources().getStringArray(R.array.main_menu);
        for (int i = 0; i < items.length; i++) {
            String item = items[i];

            Button button = new Button(this);
            button.setLayoutParams(layoutParam);
            button.setTag(i);
            button.setText(item);
            button.setOnClickListener(this);

            layout.addView(button);
        }

        setContentView(layout);

        Configuration.getInstance().setContext(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent prefIntent = new Intent(this, SettingsActivity.class);
                startActivity(prefIntent);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        final int id = (Integer) view.getTag();
        switch (id) {
            case OperationListener.MODE_SERVER:
                Intent servIntent = new Intent(this, ServerActivity.class);
                startActivity(servIntent);
                break;

            case OperationListener.MODE_FILE_TRANSFER:
                onFileTransfer();
                break;

            case OperationListener.MODE_MESSAGE_TRANSFER:
                Intent msgIntent = new Intent(this, MessagesActivity.class);
                startActivity(msgIntent);
                break;

            case OperationListener.MODE_CURSOR_CONTROL:
                Intent curIntent = new Intent(this, CursorActivity.class);
                startActivity(curIntent);
                break;

            case OperationListener.MODE_BRIGHTNESS_CHANGE:
                Intent brIntent = new Intent(this, BrightnessActivity.class);
                startActivity(brIntent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == OperationListener.MODE_FILE_TRANSFER) {
            sendFile(data.getData());
        }
    }

    private void sendFile(Uri uri) {
        if (uri == null) return;

        String path = uri.getPath();
        if (uri.getScheme().startsWith("content")) path = getRealPathFromURI(uri);

        new SocketTransferTask(OperationListener.MODE_FILE_TRANSFER).execute(path);
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, projection, null, null, null);
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }

    private void onFileTransfer() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        try {
            startActivityForResult(intent, OperationListener.MODE_FILE_TRANSFER);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, getString(R.string.app_not_found), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkSharingData() {
        Intent intent = getIntent();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(intent.getAction()) && type != null) {
            if (type.startsWith("file/") || type.startsWith("image/")) {
                Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                sendFile(uri);
                return true;
            } else if ("text/plain".equals(type)) {
                String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                new SocketTransferTask(OperationListener.MODE_MESSAGE_TRANSFER).execute(text);
                return true;
            }
        }
        return false;
    }
}
