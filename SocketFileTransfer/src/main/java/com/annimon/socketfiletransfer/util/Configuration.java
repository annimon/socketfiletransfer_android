package com.annimon.socketfiletransfer.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author aNNiMON
 */
public class Configuration {

    private static Configuration instance;

    public static synchronized Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    private Context context;
    private SharedPreferences preferences;

    private Configuration() { }

    public void setContext(Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getProperty(String key, String defaultValue) {
        if (preferences == null) return defaultValue;

        return preferences.getString(key, defaultValue);
    }

    public int getProperty(String key, int defaultValue) {
        if (preferences == null) return defaultValue;

        try {
            return preferences.getInt(key, defaultValue);
        } catch (Exception e) {
            String defValue = String.valueOf(defaultValue);
            try {
                return Integer.parseInt(preferences.getString(key, defValue));
            } catch (NumberFormatException e1) {
                return defaultValue;
            }
        }
    }
}
