package com.annimon.socketfiletransfer.util;

/**
 *
 * @author aNNiMON
 */
public class MessageHistory {

    private static String lastMessage = "";
    private static StringBuilder allText = new StringBuilder();

    public static void addMessage(String message) {
        lastMessage = message;
        allText.append(message).append("\r\n\r\n");
    }

    public static String getLastMessage() {
        return lastMessage;
    }

    public static String getAllText() {
        return allText.toString();
    }

}
