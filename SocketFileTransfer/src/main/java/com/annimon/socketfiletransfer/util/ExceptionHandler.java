package com.annimon.socketfiletransfer.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

/**
 * Handling exceptions.
 * If error is critical or must be show to user, then
 * call <b>alert</b> method.
 * In other situations call <b>log</b> method.
 *
 * @author aNNiMON
 */
public class ExceptionHandler {

    private static final boolean DEBUG = false;
    private static final String TAG = "ExceptionHandler";

    public static void alert(Context context, Exception ex) {
        alert(context, getErrorMessage(ex));
    }

    public static void alert(Context context, int resourceId) {
        alert(context, context.getString(resourceId));
    }

    public static void alert(Context context, String message) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(android.R.string.dialog_alert_title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        }).show();
    }

    public static void log(Exception ex) {
        if (DEBUG) {
            Log.e(TAG, getErrorMessage(ex));
        }
    }

    public static void log(String message) {
        if (DEBUG) {
            Log.e(TAG, message);
        }
    }

    private static String getErrorMessage(Exception ex) {
        return ex.getMessage();
    }

}