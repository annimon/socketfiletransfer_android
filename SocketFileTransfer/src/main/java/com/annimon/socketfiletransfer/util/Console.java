package com.annimon.socketfiletransfer.util;

import android.os.Handler;

import com.annimon.socketfiletransfer.ServerActivity;

/**
 * @author aNNiMON
 */
public class Console {

    private static Handler handler;
    private static StringBuilder allText = new StringBuilder();

    public static void print(String message) {
        allText.append(message);
        if (handler != null) handler.sendEmptyMessage(ServerActivity.UPDATE_CONSOLE);
    }

    public static void println(String message) {
        print(message + "\r\n");
    }

    public static String getAllText() {
        return allText.toString();
    }

    public static void setHandler(Handler handler) {
        Console.handler = handler;
    }

}
