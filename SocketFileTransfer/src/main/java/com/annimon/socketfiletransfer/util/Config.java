package com.annimon.socketfiletransfer.util;

/**
 *
 * @author aNNiMON
 */
public class Config {

    private static final String
            PORT = "port",
            IP_ADDRESS = "address",
            TRANSFER_DIR = "transfer_dir";

    public static int getPort() {
        return Configuration.getInstance().getProperty(PORT, 7119);
    }

    public static String getIpAddress() {
        String path = Configuration.getInstance().getProperty(IP_ADDRESS, "192.168.1.35");
        return path.trim();
    }

    public static String getTransferDir() {
        String path = Configuration.getInstance().getProperty(TRANSFER_DIR, "/mnt/sdcard/");
        return path.trim();
    }
}
