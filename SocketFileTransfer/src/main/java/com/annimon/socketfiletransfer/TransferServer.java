package com.annimon.socketfiletransfer;

import android.os.Handler;

import com.annimon.socketfiletransfer.util.Console;
import com.annimon.socketfiletransfer.util.ExceptionHandler;

import java.net.Socket;

/**
 *
 * @author aNNiMON
 */
public class TransferServer implements Runnable {

    private final OperationListener listener;
    private Handler handler;

    public TransferServer(Socket client) {
        listener = new OperationListener();
        listener.setSocket(client);
        printClientInfo(client);
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        try {
            listener.listenOperation();
        } catch (Exception ex) {
            ExceptionHandler.log(ex);
        }
        listener.close();
    }

    private void printClientInfo(Socket client) {
        Console.println(client.getRemoteSocketAddress().toString());
    }

}
