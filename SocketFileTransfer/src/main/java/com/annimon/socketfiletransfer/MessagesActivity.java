package com.annimon.socketfiletransfer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.annimon.socketfiletransfer.util.MessageHistory;

public class MessagesActivity extends Activity implements View.OnClickListener {

    private EditText editText;
    private TextView messagesHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        findViewById(R.id.send_text_button).setOnClickListener(this);
        editText = (EditText) findViewById(R.id.message_edit_text);
        messagesHistory = (TextView) findViewById(R.id.messages_history);

        messagesHistory.setText(MessageHistory.getAllText());
    }

    @Override
    public void onClick(View view) {
        String text = editText.getText().toString();
        new SocketTransferTask(OperationListener.MODE_MESSAGE_TRANSFER).execute(text);
        editText.setText("");

        MessageHistory.addMessage(" >> " + text);
        messagesHistory.setText(MessageHistory.getAllText());
    }
}
