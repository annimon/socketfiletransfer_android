package com.annimon.socketfiletransfer;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;

public class BrightnessActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brightness);

        SeekBar brightnessBar = (SeekBar) findViewById(R.id.brightness_bar);
        brightnessBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String value = String.valueOf(progress * 10);
                new SocketTransferTask(OperationListener.MODE_BRIGHTNESS_CHANGE).execute(value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
    
}
