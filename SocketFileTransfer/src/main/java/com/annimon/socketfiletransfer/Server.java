package com.annimon.socketfiletransfer;

import com.annimon.socketfiletransfer.util.Config;
import com.annimon.socketfiletransfer.util.ExceptionHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author aNNiMON
 */
public class Server {

    private final ServerSocket serverSocket;

    public Server() throws IOException {
        serverSocket = new ServerSocket(Config.getPort());
    }

    public void listenClients() {
        while (true) {
            try {
                Socket client = serverSocket.accept();
                new Thread(new TransferServer(client)).start();
            } catch (IOException ex) {
                ExceptionHandler.log(ex);
                break;
            }
        }
        close();
    }

    private void close() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                ExceptionHandler.log(ex);
            }
        }
    }

}
