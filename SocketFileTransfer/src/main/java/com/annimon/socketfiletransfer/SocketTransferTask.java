package com.annimon.socketfiletransfer;

import android.os.AsyncTask;

import com.annimon.socketfiletransfer.util.Config;
import com.annimon.socketfiletransfer.util.ExceptionHandler;

import java.io.IOException;
import java.net.Socket;

public class SocketTransferTask extends AsyncTask<Object, Void, Boolean> {

    /** Transfer mode */
    private int mode;

    public SocketTransferTask(int mode) {
        this.mode = mode;
    }

    @Override
    protected Boolean doInBackground(Object... objects) {
        if (mode == OperationListener.MODE_SERVER) return startServer();

        boolean success = true;
        OperationManager manager = new OperationManager();
        try {
            Socket socket = new Socket(Config.getIpAddress(), Config.getPort());
            manager.setSocket(socket);
            manager.execute(mode, objects);
        } catch (Exception e) {
            success = false;
            ExceptionHandler.log(e);
        } finally {
            if (manager != null) manager.close();
        }

        return success;
    }

    private boolean startServer() {
        try {
            Server server = new Server();
            server.listenClients();
        } catch (IOException e) {
            ExceptionHandler.log(e);
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
    }
}
