package com.annimon.socketfiletransfer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author aNNiMON
 */
public class TouchpadView extends View {

    public TouchpadView(Context context) {
        super(context);
        initView();
    }

    public TouchpadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public TouchpadView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    private void initView() {
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public void closeActivity() {
        ((Activity)getContext()).finish();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.GRAY);
    }
}
