package com.annimon.socketfiletransfer;

import android.app.Activity;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.WindowManager;
import android.widget.TextView;

import com.annimon.socketfiletransfer.operations.Operation;
import com.annimon.socketfiletransfer.util.Console;

public class ServerActivity extends Activity {

    public static final int
            UPDATE_CONSOLE = 1,
            CHANGE_BRIGHTNESS = 2;

    private TextView messagesHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        messagesHistory = (TextView) findViewById(R.id.messages_history);
        messagesHistory.setText(Console.getAllText());

        Console.setHandler(handler);
        Operation.setHandler(handler);

        WifiManager wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipAddress = Formatter.formatIpAddress(ip);
        Console.println("Start server " + ipAddress);

        new SocketTransferTask(OperationListener.MODE_SERVER).execute();
    }

    private final Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == UPDATE_CONSOLE) {
                messagesHistory.setText(Console.getAllText());
            } else if (msg.what == CHANGE_BRIGHTNESS) {
                int brightness = msg.arg1;

                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.screenBrightness = brightness / 100.0f;
                getWindow().setAttributes(lp);
            }

        }
    };

}
