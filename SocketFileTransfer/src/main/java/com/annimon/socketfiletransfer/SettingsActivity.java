package com.annimon.socketfiletransfer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * @author aNNiMON
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_main);
    }

}
